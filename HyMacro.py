import time
import pyautogui
import keyboard

class MacroManager:
    def realizar_recorrido(self, teclas, cocoa, cocoa_esperar, timing):
        pyautogui.mouseDown(button='left')
        keyboard.press(teclas[0])
        if cocoa:
            if cocoa_esperar >= 1:
                time.sleep(1)
        keyboard.press(teclas[1])
        time.sleep(timing)
        pyautogui.mouseUp(button='left')
        keyboard.release(teclas[0])
        keyboard.release(teclas[1])

    def realizar_conjunto_recorridos(self, teclas, recorridos_to_warp, cocoa_esperar, timing):
        while True:
            for _ in range(recorridos_to_warp):
                self.realizar_recorrido(teclas[0:-2], cocoa_esperar, _, timing)
                self.realizar_recorrido(teclas[2:], cocoa_esperar, 0, timing)
            pyautogui.press('t')
            pyautogui.write('/warp garden')
            pyautogui.press('enter')

    def realizar_recorrido_cobblestone(self, tecla):
        pyautogui.mouseDown(button='left')
        keyboard.press(tecla)
        time.sleep(240)
        pyautogui.mouseUp(button='left')
        keyboard.release(tecla)

    def realizar_conjunto_recorridos_cobblestone(self):
        while True:
            self.realizar_recorrido_cobblestone('w')
            pyautogui.press('t')
            pyautogui.write('/hub')
            pyautogui.press('enter')
            time.sleep(3)
            pyautogui.press('t')
            pyautogui.write('/is')
            pyautogui.press('enter')

    def run_macro(self):
        print(r'''╔═══════════════════════════════════════════════════════════════════════════════╗
║     /$$   /$$           /$$      /$$                                          ║
║    | $$  | $$          | $$$    /$$$                                          ║
║    | $$  | $$ /$$   /$$| $$$$  /$$$$  /$$$$$$   /$$$$$$$  /$$$$$$  /$$$$$$    ║
║    | $$$$$$$$| $$  | $$| $$ $$/$$ $$ |____  $$ /$$_____/ /$$__  $$/$$__  $$   ║
║    | $$__  $$| $$  | $$| $$  $$$| $$  /$$$$$$$| $$      | $$  \__/ $$  \ $$   ║
║    | $$  | $$| $$  | $$| $$\  $ | $$ /$$__  $$| $$      | $$     | $$  | $$   ║
║    | $$  | $$|  $$$$$$$| $$ \/  | $$|  $$$$$$$|  $$$$$$$| $$     |  $$$$$$/   ║
║    |__/  |__/ \____  $$|__/     |__/ \_______/ \_______/|__/      \______/    ║
║               /$$  | $$|                                                      ║
║              |  $$$$$$/                                                       ║
║               \______/                                                        ║
║                                                                               ║
║        F8: Cocoa-Beans        F9: Nether Warth        F10: Cobblestone        ║
║                                                                               ║
╚═══════════════════════════════════════════════════════════════════════════════╝''')
        while True:
            if keyboard.is_pressed('F8'):
                self.realizar_conjunto_recorridos(['w', 'd', 's', 'a'], 8, True, 93)
            if keyboard.is_pressed('F9'):
                self.realizar_conjunto_recorridos(['w', 'd', 'w', 'a'], 4, False, 119)
            if keyboard.is_pressed('F10'):
                self.realizar_conjunto_recorridos_cobblestone()
            time.sleep(0.1)

if __name__ == "__main__":
    macro_manager = MacroManager()
    macro_manager.run_macro()